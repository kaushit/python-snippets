def bind(obj, methods):
    for method in methods:
        def func(method=method):
            return method()
        setattr(obj, method.__name__, func)

def test():
    class A: pass
    def foo():
        return "FOO"
    def bar():
        return "BAR"
    a = A()
    bind(a, [foo, bar])
    assert a.bar() == "BAR"
    assert a.foo() == "FOO"

if __name__ == "__main__":
    test()
