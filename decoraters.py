def decorator_with_args(*decorator_args):
    def decorator(function):
        def wrapper(*args):
            return function(*(decorator_args+args))
        return wrapper
    return decorator

@decorator_with_args(2)
def pow(x,y):
  	return x*y

print(pow(2))

@decorator_with_args("/home", "user")
def path(*args):
   	return "/".join(args)

print(path("photos", "cats.png"))
