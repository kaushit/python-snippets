SELECT
    a.id AS account_id,
    a.name AS account_name,
    SUM(t.amount) AS total_amount_this_month
FROM
    accounts AS a
LEFT JOIN (
    SELECT
        account_id,
        amount
    FROM
        transactions
    WHERE
        created_at >= DATE_TRUNC('month', CURRENT_DATE)
        AND created_at < DATE_TRUNC('month', CURRENT_DATE) + INTERVAL '1 month'
        AND amount > 100
) AS t ON a.id = t.account_id
WHERE
    a.created_at >= DATE_TRUNC('month', CURRENT_DATE) - INTERVAL '1 month'
    AND a.created_at < DATE_TRUNC('month', CURRENT_DATE)
GROUP BY
    a.id, a.name;