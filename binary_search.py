arr = [1, 3, 4, 7, 12, 15, 19, 22]
target = 7

# through recursion
def search(arr:list, target:int, start:int, end:int) -> int:
    mid = (start + end) // 2
    mid_value = arr[mid]
    if start > end:
        return -1 

    if target == mid_value:
        return mid
    
    if target > mid_value:
        start = mid + 1
    
    if target < mid_value:
        end = mid - 1
    return search(arr, target, start, end)

print(search(arr,target, 0, len(arr)-1))


#through iteration
def search(arr:list, target:int) -> int:

    start = 0
    end = len(arr) - 1

    while start <= end:
        
        mid_index = (start + end) // 2
        mid_value = arr[mid_index]

        if mid_value == target:
            return mid_index
        elif mid_value > target:
            end = mid_index - 1
        else:
            start = mid_index + 1

    return -1

print(search(arr, target))