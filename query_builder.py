class QueryBuilder:
    def __init__(self, table_name):
        self.table_name = table_name
        self.select_fields = "*"
        self.where_conditions = []
        self.order_field = None
        self.order_direction = None
        self.limit_value = None

    def where(self, **conditions):
        for field, value in conditions.items():
            self.where_conditions.append(f"{field}={value}")
        return self

    def order(self, field, direction):
        self.order_field = field
        self.order_direction = direction
        return self

    def limit(self, value):
        self.limit_value = value
        return self

    def to_sql(self):
        query = f"SELECT {self.select_fields} FROM {self.table_name}"

        if self.where_conditions:
            query += " WHERE " + " AND ".join(self.where_conditions)

        if self.order_field and self.order_direction:
            query += f" ORDER BY {self.order_field} {self.order_direction}"

        if self.limit_value:
            query += f" LIMIT {self.limit_value}"

        return query


query = QueryBuilder("table_name").where(a=2).where(b=4).order("name", "desc").limit(5).to_sql()
print(query)